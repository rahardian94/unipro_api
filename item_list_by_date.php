<?php

function get_items_by_date($link, $start_date, $product_table, $price_table, $category_table,$product_category_table)
{
    global $mysqli;
    $dates = "?startdate=".$start_date."&enddate=".$start_date;
    $unique_date = new DateTime($start_date);
    $link = $link.$dates;
    $curling = curl_init();
    curl_setopt($curling, CURLOPT_URL, $link);
    curl_setopt($curling, CURLOPT_RETURNTRANSFER, true);
    echo "Curling the API link: ".$link."\n";
    $result = curl_exec($curling);
    echo "API executed.. Decoding JSON.. \n";
    $response_code = curl_getinfo($curling, CURLINFO_HTTP_CODE);
    $json = json_decode($result, true);
    $json = array($json);
    echo $link." get response code: ".$response_code."\n";
    echo count($json[0]['data'])." records\n";
    curl_close($curling);
    $count_success = 0;
    $count_error = 0;
    $count_created = 0;
    $num = 0;
    if($json[0]['data'] == NULL)
    {
        echo "No product updates for today\n";
    }
    else
    {
        $query_list_id = "SELECT product_code FROM ".$product_table;
        $list_product = $mysqli->query($query_list_id);
        $list_product = $list_product->fetch_all();

        foreach ($json[0]['data'] as $data)
        {
            if(in_array($data['Product_Id'], $list_product))
            {
                $query_update_product = "UPDATE ".$product_table." SET product_name ='".$mysqli->real_escape_string($data['Title'])."', product_sort_price = ".$data['Sales_Price']." WHERE product_id = '".$mysqli->real_escape_string($data['Product_Id'])."'";
                if($mysqli->query($query_update_product) === TRUE)
                {
                    $count_success++;
                }
                else
                {
                    $count_error++;
                }
            }
            else
            {
                $unique_date->modify('+'.$num++.' seconds');
                create_if_not_exist($data, $category_table, $product_table, $price_table, $product_category_table, $unique_date, $num);
                $count_created++;
            }
        }
        echo $count_success." products are successfully updated\n";
        echo $count_created." new products are successfully created\n";
        echo $count_error." products got error when updating\n";
    }
}

function create_if_not_exist($data, $category_table, $product_table, $price_table, $product_category_table,$unique_date,$num)
{
    global $mysqli;
    $category_id_query = "SELECT * FROM ".$category_table." WHERE `category_name` = '".$mysqli->real_escape_string($data['Category'])."'";
    $category_id_query = $mysqli->query($category_id_query);
    $category_id = $category_id_query->fetch_assoc();
    if($category_id_query->num_rows == 0)
    {
        $insert_category_query = "INSERT INTO ".$category_table." (category_parent_id, category_type, category_published, category_namekey, category_depth, category_name) VALUES(2,'product',1,'product_".$unique_date->getTimestamp()."_".$unique_date->getTimestamp()."',1,'".$mysqli->real_escape_string($data['Category'])."')";
        $mysqli->query($insert_category_query);
        $cid = $mysqli->insert_id;
    }
    else
    {
        $cid = $category_id['category_id'];
    }
    $subcategory_id_query = "SELECT * FROM ".$category_table." WHERE `category_name` = '".$data['Sub_Category']."' AND category_parent_id = ".$cid;
    $subcategory_id_query = $mysqli->query($subcategory_id_query);
    $subcategory_id = $subcategory_id_query->fetch_assoc();
    if($subcategory_id_query->num_rows == 0)
    {
        $unique_date->modify('+'.$num++.' seconds');
        $insert_subcategory_query = "INSERT INTO ".$category_table." (category_parent_id, category_type, category_published, category_namekey, category_depth,  category_name) VALUES(".$cid.",'product',1,'product_".$unique_date->getTimestamp()."_".$unique_date->getTimestamp()."',2,'".$mysqli->real_escape_string($data['Sub_Category'])."')";
        $mysqli->query($insert_subcategory_query);
        $scid = $mysqli->insert_id;
    }
    else
    {
        $scid = $subcategory_id['category_id'];
    }
    $create_date = new DateTime(str_replace('T',' ',$data['CreateDate']));
    $modify_date = new DateTime(str_replace('T',' ',$data['ModifyDate']));  
    $insert_product_query = "INSERT INTO ".$product_table." (product_name,product_description,product_code,product_sort_price,product_published,product_type,product_condition,product_created,product_modified) VALUES ('".$mysqli->real_escape_string($data['Title'])."','".$mysqli->real_escape_string($data['Description'])."','".$mysqli->real_escape_string($data['Product_Id'])."',".$data['Sales_Price'].",1,'main','NewCondition',".$create_date->getTimestamp().",".$modify_date->getTimestamp().")";
    $mysqli->query($insert_product_query);
    $id = $mysqli->insert_id;

    $insert_product_category_query = "INSERT INTO ".$product_category_table." (category_id,product_id,ordering) VALUES (".$scid.",".$id.",1)";
    $mysqli->query($insert_product_category_query);
    $insert_product_price_query = "INSERT INTO ".$price_table." (price_currency_id,price_product_id,price_value) VALUES(140,".$id.",".$data['Sales_Price'].")";
    $mysqli->query($insert_product_price_query);
}

?>