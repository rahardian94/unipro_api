<?php

function link_image($prefix_db, $folder_image)
{
    global $mysqli;
    $files_query = "SELECT product_id,product_code FROM ".$prefix_db."_hikashop_product where product_id between 1 and 8";
    $files_query = $mysqli->query($files_query);
    $files = $files_query->fetch_all();
    $directory = dirname(__FILE__).$folder_image;
    echo "Inserting images..\n";
    $x = 0;
    $y = 0;
    foreach($files as $f)
    {
        $count = 0;
        $list_image = scandir($directory.$f[1]);
        $list_image = array_slice($list_image,2);
        foreach($list_image as $image)
        {
            $insert_image_query = "INSERT INTO ".$prefix_db."_hikashop_file (file_name, file_path, file_type, file_ref_id, file_ordering) VALUES('".$mysqli->real_escape_string(substr($image, 0,(strlen($image)-4)))."','".$mysqli->real_escape_string($folder_image.$f[1]."\\".$image)."','product',".$f[0].",".$count.")";
            if(!$mysqli->query($insert_image_query))
            {
                echo "Error detected: ".$mysqli->error."\n";
                $y++;
            }
            else
            {
                $x++;
            }
            $count++;
        }
    }
    echo "Done\n";
}



/* Konfigurasi DB, isi sesuai kebutuhan */
$server = "localhost";
$user = "root";
$pass = "";
$dbase = "hikashops";
$mysqli = new mysqli($server,$user,$pass,$dbase);

if ($mysqli->connect_error) {
    die("Connection failed: " . $mysqli->connect_error);
} 
echo "Database Connected successfully\n";

/* untuk prefix database dan direktori image diisi sesuai konfigurasi di server atau local */
$prefix_db ='ty08n'; //prefix joomla
$folder_image = "\\product\\"; //nama folder image

link_image($prefix_db,$folder_image);
$mysqli->close();
?>