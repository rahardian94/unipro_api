<?php

function send_post_sales_order($link, $order_table, $order_product_table, $address_table, $order_id)
{
    global $mysqli;
    $query = "SELECT op.order_product_code, op.order_product_name, op.order_product_price, o.order_number, o.order_invoice_created, o.order_full_price, op.order_product_tax, a.address_user_id,
    a.address_firstname, a.address_middle_name, a.address_lastname, a.address_street, a.address_street2, a.address_telephone, a.address_fax FROM ".$order_table." AS o LEFT JOIN ".$order_product_table." AS op ON op.order_id = o.order_id LEFT JOIN ".$address_table." AS a ON a.address_user_id = o.order_user_id WHERE o.order_id = ".$order_id;
    $p = $mysqli->query($query);
    $table = $p->fetch_assoc();
    if(!empty($table['address_middle_name'])) $table['address_middle_name'] .= " ";

    $data = array(
        "detail" => array([
            "SlNo" => 0,
            "InventoryCode" => (!empty($table['order_product_code'])) ? $table['order_product_code'] : "",
            "Description" => (!empty($table['order_product_name'])) ? $table['order_product_name'] : "",
            "wqty" => 0,
            "wunit" => 0,
            "lqty" => 0,
            "unitprice" => (!empty($table['order_product_price'])) ? (float)$table['order_product_price'] : 0,
            "discount" => 0,
            "totalvalue" => 0,
            "focqty" => 0
        ]),
        "header" => array(
            "OnlineSalesOrderNo" => (!empty($table['order_number'])) ? $table['order_number'] : "", // ini order no dr hikashop
            "InvoiceDate" => (!empty($table['order_invoice_created'])) ? gmdate("Y-m-d\TH:i:s\Z", $table['order_invoice_created']) : "",  //tgl invoice date dr hikashop
            "TotalValue" => (!empty($table['order_full_price'])) ? (float)$table['order_full_price'] : 0, //total value
            "Gst" => (!empty($table['order_product_tax'])) ? (float)$table['order_product_tax'] : 0, //gst tax
            "discount" => 0,
            "SubTotal" => 0,
            "CustomerCode" => (!empty($table['address_user_id'])) ? $table['address_user_id'] : "", //does this customer has the code
            "CustomerName" => $table['address_firstname']." ".$table['address_middle_name'].$table['address_lastname'], //does this customer has name
            "Address1" => (!empty($table['address_street'])) ? $table['address_street'] : "", //alamat 1
            "Address2" => (!empty($table['address_street'])) ? $table['address_street'] : "", //alamat 2
            "Address3" => "", //alamat 3
            "Phone" => (!empty($table['address_phone'])) ? $table['address_phone'] : "", 
            "Fax" => (!empty($table['address_fax'])) ? $table['address_fax'] : "",
            "GstType" => "",
            "ShippingSlNo" => "",
            "ShippingTo" => (!empty($table['address_user_id'])) ? $table['address_user_id'] : "", //nama cust
            "ShippingAddr1" => "", //alamat shipping 1
            "ShippingAddr2" => "", //alamat shipping 2
            "ShippingPhone" => "" //shipping no telepone
        )
        );
       
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $link,
        CURLOPT_POST => 1,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json' ,
            'Accept: application/json'
    
        ),
        CURLOPT_POSTFIELDS => json_encode($data)
    ));
    $result = curl_exec($curl);
    $response_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if($response_code >= 200 && $response_code < 400)
    {
        echo "Response code: ".$response_code."\n";
        echo "Data sent successfully\n";
    }
    curl_close($curl);
}
?>