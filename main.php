<?php

require "dbconn.php";
require "item_list.php";
require "stock_details.php";
require "item_list_by_date.php";
require "post_sales_order.php";



$start_date = '2017-12-25';
$end_date = '2019-01-07';

$item_list = 'http://unipro.myvnc.com:65/api/ItemList';
$items_byDate = 'http://unipro.myvnc.com:65/api/Items_ByDate';
$stock_details = 'http://unipro.myvnc.com:65/api/StockDetails';
$post_sales_order = 'http://unipro.myvnc.com:65/api/PostSalesOrder';
$order_id = 1; //order
$prefix = 'ty08n_hikashop_'; // change prefix tablecomm

get_item_list($item_list, $prefix.'product', $prefix.'product_category', $prefix.'price', $prefix.'category');
get_items_by_date($items_byDate, date("Y-m-d"), $prefix.'product', $prefix.'price', $prefix.'category', $prefix.'product_category');
get_stock_details($stock_details, $prefix.'product');
send_post_sales_order($post_sales_order, $prefix.'order', $prefix.'order_product', $prefix.'address', $order_id);

$mysqli->close();
?>