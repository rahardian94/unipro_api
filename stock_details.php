<?php



function get_stock_details($link,$product_table)
{
    global $mysqli;
    $curling = curl_init();
    curl_setopt($curling, CURLOPT_URL, $link);
    curl_setopt($curling, CURLOPT_RETURNTRANSFER, TRUE);
    echo "Curling the API link: ".$link."\n";
    $data = curl_exec($curling);
    echo "API Executed.. Decoding JSON.. \n";
    $response_code = curl_getinfo($curling, CURLINFO_HTTP_CODE);
    $json = json_decode($data, true);
    $json = array($json);
    echo $link." get response code: ".$response_code."\n";
    curl_close($curling);
    $count = 0;
    $count_error = 0;
    foreach($json[0]['values'] as $value)
    {
        $query = "UPDATE ".$product_table." SET product_quantity = ".$value['QtyOnHand']." WHERE product_code = '".$mysqli->real_escape_string($value['InventoryCode'])."'";
        if($mysqli->query($query) == TRUE)
        {
            $count++;
        }
        else
        {
            $count_error++;
            echo $mysqli->error."\n";
        }
    }
    echo "Process Finished..\n".$count." records saved successfully\n".$count_error." records got errors\n";

    echo count(minus_amount_product($product_table))." get minus amount.. please check..\n";
}

function minus_amount_product($table)
{
    global $mysqli;
    $query = "SELECT product_code, product_quantity FROM ".$table." where product_quantity < 0";
    $result = $mysqli->query($query);
    return $result->fetch_all();
}

?>