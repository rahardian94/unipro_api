<?php

function get_item_list($link, $product_table, $product_category_table, $price_table, $category_table)
{
    global $mysqli;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    echo "Curling the API link: ".$link."\n";
    $data = curl_exec($ch);
    echo "Executed.. Decoding JSON.. \n";
    $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $json = json_decode($data, true);
    $json = array($json);
    $files = fopen('item_list.json','w');
    fwrite($files, $data);
    fclose($files);
    curl_close($ch);  
    echo $link." get response code: ".$response_code."\n";
    $count = 0;
    $count_error = 0;
    $counts = 0;
    $num = 0;
    $categories = array();
    $sub_category = array();
    $get_data = $json[0]['data'];
    echo "There are ".count($get_data)." records\n";
    // print_r($get_data[33]);
    echo "Inserting categories..\n";
    for($i=0;$i<count($get_data);$i++)
    {
        $sub_category[$i] = array(
            'Category' => $get_data[$i]['Category'],
            'Sub_Category' => $get_data[$i]['Sub_Category']
        );
        $categories[$i] = $get_data[$i]['Category'];
    }
    
    $categories = array_unique($categories);
    $sub_category = array_unique($sub_category, SORT_REGULAR);
    $dates = new DateTime(date('Y-m-d H:i:s'));

    foreach ($categories as $cat)
    {
        $dates->modify('+'.$num++.' seconds');
        $query3 = "INSERT INTO ".$category_table." (`category_parent_id`,`category_type`,`category_name`,`category_published`,`category_namekey`,`category_alias`,`category_depth`,`category_ordering`) VALUES (2,'product','".$mysqli->real_escape_string($cat)."',1,'product_".$dates->getTimestamp()."_".$dates->getTimestamp()."','".$mysqli->real_escape_string($cat)."',2,1)";
        if(!$mysqli->query($query3))
        {
            echo $mysqli->error." detected";
        }
    }

    foreach ($sub_category as $value)
    {
        $dates->modify('+'.$num++.' seconds');
        $category_parent_query = "SELECT `category_id` FROM ".$category_table." WHERE `category_name` = '".$mysqli->real_escape_string($value['Category'])."'";
        $category_parent_query = $mysqli->query($category_parent_query);
        $category_parent = $category_parent_query->fetch_assoc();
        $sub_query = "INSERT INTO ".$category_table."(`category_parent_id`,`category_type`,`category_name`,`category_published`,`category_namekey`,`category_alias`,`category_depth`,`category_ordering`) VALUES (".$category_parent['category_id'].",'product','".$mysqli->real_escape_string($value['Sub_Category'])."',1,'product_".$dates->getTimestamp()."_".$dates->getTimestamp()."','".$mysqli->real_escape_string($value['Sub_Category'])."',3,1)";
        if(!$mysqli->query($sub_query))
        {
            echo $mysqli->error." detected";
        }
    }

    echo "Inserting Product and Price..\n";
    foreach ($json[0]['data'] as $value)
    {
        // $dates->modify('+'.$num++.' seconds');
        $create_date = new DateTime(str_replace('T',' ',$value['CreateDate']));
        $modify_date = new DateTime(str_replace('T',' ',$value['ModifyDate']));
        $query = "INSERT INTO ".$product_table."(`product_name`,`product_description`,`product_code`,`product_sort_price`,`product_published`,`product_type`,`product_condition`,`product_created`,`product_modified`) VALUES ('".$mysqli->real_escape_string($value['Title'])."','".$mysqli->real_escape_string($value['Description'])."','".$mysqli->real_escape_string($value['Product_Id'])."',".$value['Sales_Price'].",1,'main','NewCondition',".$create_date->getTimestamp().",".$modify_date->getTimestamp().")";
        $mysqli->query($query);

        $last_id = $mysqli->insert_id;
        $query2 = "INSERT INTO ".$price_table."(`price_currency_id`,`price_product_id`,`price_value`) VALUES (140,".$last_id.",".$value['Sales_Price'].")";

        if($mysqli->query($query2) === TRUE)
        {
            // echo $count." . ".$value['Tag']." successfully inserted\n";
            $count += 1;
        }
        else
        {
            $count_error += 1;
            // echo "Error : ".$query."\n".$mysqli->error."\n";    // Thanks to Pekka for pointing this out.
        }
        // $dates->modify('+'.$num++.' seconds');
    }
    echo $count." products saved successfully\n";
    echo $count_error." products get error\n";

    echo "Inserting product category..\n";
    $x = 0;
    $y = 0;
    foreach ($json[0]['data'] as $value)
    {
        $category_id_query = "SELECT * FROM ".$category_table." WHERE `category_name` = '".$mysqli->real_escape_string($value['Category'])."'";
        $category_id_query = $mysqli->query($category_id_query);
        $category_id = $category_id_query->fetch_assoc();
        $subcategory_id_query = "SELECT * FROM ".$category_table." WHERE `category_name` = '".$mysqli->real_escape_string($value['Sub_Category'])."' AND category_parent_id = ".$category_id['category_id'];

        $subcategory_id_query = $mysqli->query($subcategory_id_query);
        $subcategory_id = $subcategory_id_query->fetch_assoc();

        $product_id_query = "SELECT * FROM ".$product_table." WHERE `product_code` = '".$mysqli->real_escape_string($value['Product_Id'])."'";
        $product_id_query = $mysqli->query($product_id_query);
        $product_id = $product_id_query->fetch_assoc();

        // echo $subcategory_id['category_id']." - ".$product_id['product_id']."\n";
        $query4 = "INSERT INTO ".$product_category_table." (`category_id`,`product_id`,`ordering`) VALUES (".$subcategory_id['category_id'].",".$product_id['product_id'].",1)";

        if($mysqli->query($query4) === TRUE)
        {
            $x++;
        }
        else
        {
            // echo "Error detected: ".$mysqli->error;
            $y++;
        }
    }
    echo $x." product category records saved successfully\n";
    echo $y." product category records get errors\n";
    echo "Donee :)\n";
}
?>